const { gql } = require("apollo-server");

// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`
  type User {
    id:ID,
    name: String,
    email: String,
    mobile: String,
  }  

  type Query {
    Users:[User],
    SelectedUser(id:ID):User
  }
  type Mutation{
    AddUser(id:String, name:String, email:String, mobile:String):User
    DeleteUser(id:ID):User
    updateUser(name:String):User
  }
`;
module.exports = typeDefs;
