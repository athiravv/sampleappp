import React, { Component } from 'react';
import {Text,StyleSheet,Button, View,TextInput,Image,StatusBar} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { gql, useQuery } from '@apollo/client';
import { AppLoading } from 'react';


const getAllUsers = gql`
query {
  Users{
    id,
    name,
    email,
    mobile 
   }
}
`

const ChapterItem = ({ chapter, onPress }) => {
  const { name, mobile } = chapter
  

  return (
    <Pressable style={styles.item} onPress={onPress}>
      <Text style={styles.header}>{name}</Text>
      {mobile && <Text style={styles.subheader}>{mobile}</Text>}
    </Pressable>
  )
}


export default class Createuser extends Component {
    render() {
      const { data, loading } = useQuery(getAllUsers)


  return (
    <FlatList
      data={data.Users}
      renderItem={({ item }) => (
        <ChapterItem
          chapter={item}
          onPress={() => navigation.navigate('Chapter', { chapter: item })}
        />
      )}
      keyExtractor={(chapter) => chapter.id.toString()}
      
    />
    
  )
  
      // return (
      //         <View style={styles.container}>
      //           <View style={styles.header}>
      //               <Text style={styles.headertext}>User List</Text>
      //           </View>
      //           <View style={styles.viewbutton}>
      //                  <Button title="Create User" onPress={() =>
      //             this.props.navigation.navigate('Createuser')} ></Button>

      //           </View> 
      //           <View style={styles.body}>
      //           <TouchableOpacity style={{paddingHorizontal:wp("40%"),}} onPress={() =>
      //             this.props.navigation.navigate('Userinfo')}>
      //      <Text  style={{color:"white",letterSpacing:1.5,fontWeight:"bold",fontSize:hp("2.5%"),marginLeft:-wp("3%")}}>User1</Text></TouchableOpacity>
      //           </View>
                


      //           </View>
              

      //       )
    }
  }
  const styles = StyleSheet.create({

    header:{
        marginLeft:'3%',
        marginTop:'4%',
        flexDirection:"row"
      },
      headertext:{
        marginLeft:'3%',
        marginBottom:20,
        color:'#141823',
        fontSize:17,
      
      },
      viewbutton:{
        flexDirection:"row",
        alignItems:"center"
      },



})