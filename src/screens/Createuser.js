import React, { Component } from 'react';
import {Text,StyleSheet,Button, View,TextInput,Image,StatusBar} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Createuser extends Component {
    render() {
      return (
              <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headertext}>Create User</Text>
                </View>
                <View style={styles.body}>
                    <View style={styles.nameview}>
                        <Text style={styles.textname}>ID:</Text>
                        <TextInput style={styles.viewtouch}></TextInput>
                    </View>
                    <View style={styles.numberview}>
                        <Text style={styles.textnum}>NAME:</Text>
                        <TextInput style={styles.viewnum}></TextInput>
                    </View>
                    <View style={styles.emailview}>
                        <Text style={styles.textemail}>Email:</Text>
                        <TextInput style={styles.viewemail}></TextInput>
                    </View>
                    <View style={styles.addressview}>
                        <Text style={styles.textadd}>Mobile:</Text>
                        <TextInput style={styles.viewaddress}></TextInput>
                    </View>
                     <View style={styles.viewbutton}>
                       <Button title="Create User"   ></Button>

                     </View>
                    
                </View>
                </View>

            )
    }
  }
  const styles = StyleSheet.create({
      
    header:{
      marginLeft:'3%',
      marginTop:'4%',
      flexDirection:"row"
    },
    headertext:{
      marginLeft:'3%',
      marginBottom:20,
      color:'#141823',
      fontSize:17,
    
    },
    nameview:{
      flexDirection:"row",
      alignItems:"center"
    },
    textname:{
      marginLeft:10,
      marginTop:-5
    },
    viewtouch:{
      height:40,
      marginTop:20,
      marginLeft:30,
      width:"75%",
      borderColor:'blue',
      borderWidth:1,
      marginBottom:20
    },
    numberview:{
      flexDirection:"row",
      alignItems:"center"
    },
    textnum:{
      marginLeft:10,
      marginTop:-5
    },
    viewnum:{
      height:40,
      marginTop:20,
      marginLeft:20,
      width:"74%",
      borderColor:'blue',
      borderWidth:1,
      marginBottom:20
    },
    emailview:{
      flexDirection:"row",
      alignItems:"center"
    },
    textemail:{
      marginLeft:10,
      marginTop:-5
    },
    viewemail:{
      height:40,
      marginTop:20,
      marginLeft:33,
      width:"75.5%",
      borderColor:'blue',
      borderWidth:1,
      marginBottom:20
    },
    addressview:{
      flexDirection:"row",
      alignItems:"center"
    },
    textadd:{
      marginLeft:10,
      marginTop:-5
    },
    viewaddress:{
      height:40,
      marginTop:20,
      marginLeft:20,
      width:"74%",
      borderColor:'blue',
      borderWidth:1,
      marginBottom:20
    },
    viewbutton:{
      flexDirection:"row",
      alignItems:"center"
    },
  buttontext:{
    marginTop:20,
      marginLeft:20,
  },

})