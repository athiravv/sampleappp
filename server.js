
const { ApolloServer, gql } = require("apollo-server");
const typeDefs = require("./src/graphql/schema");
const user = require("./src/user");
const client= require("./src/db")



// Resolvers define the technique for fetching the types defined in the
// schema. This resolver retrieves books from the "books" array above.
const resolvers = {
  Query: {
    Users: ()=> user.getAllUsers(),
    SelectedUser:(_,args)=> user.getSelectedUser(args),
  },
  Mutation: {
    AddUser: (_,args)=>user.createUser(args),
    DeleteUser: (_,args)=>user.userDelete(args),
    updateUser:(_,args)=>user.updateUser(args),
  },
};

client.connect()
.then(() => console.log("connected sucessfully"))
.catch( e => console.log (e))

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers });
// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});