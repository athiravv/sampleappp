
import React,{useState, useEffect} from 'react';

import{
  SafeAreaView, 
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar
} from 'react-native';
import Profile from "./src/components/Profile";
import Createuser from "./src/screens/Createuser";
import Homepage from "./src/screens/Homepage";
import Userinfo from "./src/screens/Userinfo";
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer} from '@react-navigation/native';
import { AppRegistry } from 'react-native';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import AsyncStorage from '@react-native-community/async-storage';
import { persistCache } from 'apollo3-cache-persist';
import { AppLoading } from 'react';
import { useAuth0 } from '@auth0/auth0-react';



const Stack = createStackNavigator()

const cache = new InMemoryCache()
// Initialize Apollo Client
const client = new ApolloClient({
  uri: 'http://localhost:4000/',
  cache,
  defaultOptions: { watchQuery: { fetchPolicy: 'cache-and-network' } },
})

export default function App() {
  const [loadingCache, setLoadingCache] = useState(true)

  useEffect(() => {
    persistCache({
      cache,
      storage: AsyncStorage,
    }).then(() => setLoadingCache(false))
  }, [])

  if (loadingCache) {
    return <AppLoading />
  }
  //Auth0 AUthentication
  // function App() {
  //   const { isLoading } = useAuth0();
  
  //   if (isLoading) return <div>Loading...</div>
  return (
    <ApolloProvider client={client}>
    < NavigationContainer>
 <Stack.Navigator>
      <Stack.Screen options={{headerShown:false}} name="Homepage" component={Homepage} />
      <Stack.Screen options={{headerShown:false}} name="Createuser" component={Createuser} />
      <Stack.Screen options={{headerShown:false}} name="Userinfo" component={Userinfo} />
     
    </Stack.Navigator>
  </NavigationContainer>
  </ApolloProvider>
  )
  }
//}